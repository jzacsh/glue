export function assert<T>(subject: T, failMessage: string): T {
  if (subject) return subject;

  const errorMsg = `assert failed: ${failMessage}`;
  throw new Error(errorMsg);
}

export function appendNewEl(parent: Element, nodeName: string, attrs: {[attr: string]: string} = {}): Element {
  const newEl = buildEl(nodeName, attrs);
  parent.appendChild(newEl);
  return newEl;
}

export function buildEl(nodeName: string, attrs: {[attr: string]: string} = {}): Element {
  const el = document.createElement(nodeName);
  for (let attr in attrs) {
    e.setAttribute(attr, attrs[attr]);
  }
  return el;
}
